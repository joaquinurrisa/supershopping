package com.example.supershopping.fakedatabase

import com.example.supershopping.model.Item
import com.example.supershopping.network.SuperProduct
import com.example.supershopping.network.asItemList

object DataProvider {
    private var itemList: List<Item> = listOf()


    fun getItems() : List<Item> {
        return itemList
    }

    fun updateList(newList : List<SuperProduct>){
        itemList = newList.asItemList()
    }

}