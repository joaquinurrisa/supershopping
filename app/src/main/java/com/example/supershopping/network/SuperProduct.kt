package com.example.supershopping.network

import com.example.supershopping.model.Item
import com.example.supershopping.model.ItemType

data class SuperProduct(
    val id: Int,
    val name: String,
    val price: Float,
    val category: String,
    val listImageUrl: String,
    val checkoutImageUrl: String,
)

fun SuperProduct.toItem(): Item {
    val type = when (this.category) {
        "fruits" -> ItemType.FRUIT
        "veggies" -> ItemType.VEGGIE
        else -> ItemType.EXTRA
    }
    return Item(this.id, this.name, this.price, type, this.listImageUrl, this.checkoutImageUrl)
}

fun List<SuperProduct>.asItemList(): List<Item> {
    return this.map { it.toItem() }
}