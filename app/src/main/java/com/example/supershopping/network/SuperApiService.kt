package com.example.supershopping.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

private const val BASE_URL = "https://mobile-api.inhouse.decemberlabs.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface SuperApiService {

    @Headers("Authorization: Bearer 0a41c523-fa00-418a-a585-7dd1fc5f18e4")
    @GET("products")
    fun getProductsAsync():
            Deferred<List<SuperProduct>>
}

object SuperApi {
    val retrofitService: SuperApiService by lazy {
        retrofit.create(SuperApiService::class.java)
    }
}