package com.example.supershopping.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.supershopping.R
import kotlinx.android.synthetic.main.fragment_banner_slide_page.*


class BannerSlidePageFragment : Fragment() {

    companion object {
        const val ARG_DRAWABLE_ID = "arg_drawable_id"
        const val ARG_TITLE = "arg_title"
        const val ARG_SUBTITLE = "arg_subtitle"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_banner_slide_page, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_DRAWABLE_ID) }?.apply {
            banner_slide_page_image.setImageDrawable(ContextCompat.getDrawable(requireContext(), getInt(
                ARG_DRAWABLE_ID
            )))
        }
        arguments?.takeIf { it.containsKey(ARG_TITLE) }?.apply {
            banner_slide_page_title.text = getString(ARG_TITLE)
        }
        arguments?.takeIf { it.containsKey(ARG_SUBTITLE) }?.apply {
            banner_slide_page_subtitle.text = getString(ARG_SUBTITLE)
        }
    }
}