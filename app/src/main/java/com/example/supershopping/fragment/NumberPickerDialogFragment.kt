package com.example.supershopping.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.NumberPicker
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.supershopping.R
import com.example.supershopping.viewmodel.HomeViewModel
import kotlinx.android.synthetic.*


class NumberPickerDialogFragment(val itemId:Int,val initialQuantity:Int): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        val inflater = requireActivity().layoutInflater

        val view: View = inflater.inflate(R.layout.cart_number_picker, null) as View


        // Initialize the pickers
        val picker = view.findViewById<NumberPicker>(R.id.numberPicker1)
        val values:Array<String> = listOf(0..99).flatten().map { it.toString() }.toTypedArray()
        picker.displayedValues = values
        picker.minValue = 0
        picker.maxValue = values.size - 1
        picker.value = initialQuantity
        picker.wrapSelectorWheel = false

        if(picker.parent != null){
            (picker.parent as ViewGroup).removeView(picker)
        }

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setView(picker)

            builder.setMessage(getString(R.string.dialog_number_picker_text))
                .setPositiveButton(R.string.modify_quantity,
                    DialogInterface.OnClickListener { _, _ ->
                        // FIRE ZE MISSILES!
                        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
                        homeViewModel.onClickModifyQuantity(itemId,picker.value)
//                        Log.i("NumberPicker","Value: ${picker.value}")
                    })
                .setNegativeButton(R.string.cancel,
                    DialogInterface.OnClickListener { _, _ ->
                        // User cancelled the dialog
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}