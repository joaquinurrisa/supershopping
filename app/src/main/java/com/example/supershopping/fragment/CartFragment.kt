package com.example.supershopping.fragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.supershopping.R
import com.example.supershopping.adapter.CartItemAdapter
import com.example.supershopping.adapter.CartItemListener
import com.example.supershopping.databinding.FragmentCartBinding
import com.example.supershopping.model.CheckoutItem
import com.example.supershopping.viewmodel.CartViewModel
import com.example.supershopping.viewmodel.HomeViewModel

class CartFragment: Fragment() {
    private lateinit var cartViewModel: CartViewModel

    private lateinit var binding: FragmentCartBinding

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.action_gotocart)
        item.isVisible = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setHasOptionsMenu(true)

        binding  = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_cart,
            container,
            false
        )

        cartViewModel = ViewModelProvider(this).get(CartViewModel::class.java)


        cartViewModel.total.observe(viewLifecycleOwner, Observer {
            binding.total = "$${cartViewModel.total.value}"
        })

        val adapter: CartItemAdapter = CartItemAdapter(object : CartItemListener {
            override fun onItemClicked(chk_item: CheckoutItem) {
                NumberPickerDialogFragment(chk_item.item.id,chk_item.quantity).show(childFragmentManager,"CartNumberPickerDialog")
            }
        })
        binding.itemList.adapter = adapter

        cartViewModel.items.observe(viewLifecycleOwner, Observer {
            it?.let {
                val cartList = cartViewModel.getCartList(it)
                adapter.submitList(cartList)
                binding.buttonCheckout.isEnabled = cartList.isNotEmpty()

            }
        })



        val manager = GridLayoutManager(activity, 2)
        binding.itemList.layoutManager = manager

        binding.buttonCheckout.setOnClickListener { view:View ->
            cartViewModel.onClickCheckout()
            Toast.makeText(context,R.string.toast_text_purchase,Toast.LENGTH_SHORT).show()
            Navigation.findNavController(view).navigate(R.id.action_cartFragment_to_homeFragment)
        }

        return binding.root
    }



}