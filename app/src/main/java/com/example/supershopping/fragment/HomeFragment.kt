package com.example.supershopping.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.databinding.DataBindingUtil

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.supershopping.R
import com.example.supershopping.adapter.DataItem
import com.example.supershopping.adapter.HomeItemAdapter
import com.example.supershopping.adapter.ItemListener
import com.example.supershopping.databinding.FragmentHomeBinding
import com.example.supershopping.model.ItemType
import com.example.supershopping.viewmodel.HomeViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var binding: FragmentHomeBinding

    private lateinit var viewPager: ViewPager2

    companion object {
        private const val NUM_PAGES = 4
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )

        // Instantiate a ViewPager2 and a PagerAdapter.
        viewPager = binding.bannerViewPager

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = ScreenSlidePagerAdapter(requireActivity())
        viewPager.adapter = pagerAdapter
        val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            //  Code to be executed when viewPager changes
        }
        viewPager.registerOnPageChangeCallback(pageChangeCallback)
        binding.bannerViewPagerIndicator.setUpWithViewPager2(viewPager)



        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.homeViewModel = homeViewModel

        val adapter: HomeItemAdapter = HomeItemAdapter(object : ItemListener {
            override fun onPlus(itemId: Int) {
                homeViewModel.onClickPlus(itemId)
            }

            override fun onMinus(itemId: Int) {
                homeViewModel.onClickMinus(itemId)
            }

        })
        binding.itemList.adapter = adapter

        homeViewModel.items.observe(viewLifecycleOwner, Observer { list ->
            list?.let {
                homeViewModel.filterList()
                adapter.submitList(homeViewModel.addHeaders(homeViewModel.filteredItems!!))
//                adapter.notifyDataSetChanged()
            }
        })

        homeViewModel.filter.observe(viewLifecycleOwner, Observer {
            homeViewModel.filterList()
            adapter.submitList(homeViewModel.addHeaders(homeViewModel.filteredItems!!))
        })

        binding.searchBar.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener{
            override fun onQueryTextChange(query: String?): Boolean {
//                if (query.isNullOrBlank()) homeViewModel.setFilter("")
                homeViewModel.setFilter(query?:"")
                return false
            }

            override fun onQueryTextSubmit(newText: String?): Boolean {
                homeViewModel.setFilter(newText?:"")
                return true
            }
        })


        return binding.root
    }

    /**
     * A simple pager adapter that represents FeatureOnboardingSlidePageFragment objects, in
     * sequence.
     */
    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment  {
            val fragment = BannerSlidePageFragment()
            fragment.arguments = Bundle().apply {
                putInt(BannerSlidePageFragment.ARG_DRAWABLE_ID, when (position) {
                    0 -> R.drawable.img_banner_banana
                    1 -> R.drawable.img_banner_pomelo
                    2 -> R.drawable.img_banner_cucumber
                    else -> R.drawable.img_banner_kiwi
                })
                putString(BannerSlidePageFragment.ARG_TITLE, when (position) {
                    0 -> getString(R.string.banner_banana_title)
                    1 -> getString(R.string.banner_pomelo_title)
                    2 -> getString(R.string.banner_cucumber_title)
                    else -> getString(R.string.banner_kiwi_title)
                })
                putString(BannerSlidePageFragment.ARG_SUBTITLE, when (position) {
                    0 -> getString(R.string.banner_banana_subtitle)
                    1 -> getString(R.string.banner_pomelo_subtitle)
                    2 -> getString(R.string.banner_cucumber_subtitle)
                    else -> getString(R.string.banner_kiwi_subtitle)
                })
            }
            return fragment
        }
    }
}

