package com.example.supershopping.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.supershopping.databinding.HeaderBinding
import com.example.supershopping.databinding.ListItemQuantityBinding
import com.example.supershopping.model.CheckoutItem
import java.lang.ClassCastException

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_ITEM = 1

class HomeItemAdapter(val clickListener: ItemListener) : ListAdapter<DataItem, RecyclerView.ViewHolder>(HomeItemDiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> TextViewHolder.from(parent)
            ITEM_VIEW_TYPE_ITEM -> ViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            is DataItem.ProductItem -> ITEM_VIEW_TYPE_ITEM
            else -> throw ClassCastException("Unknown type")
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                val productItem = getItem(position) as DataItem.ProductItem
                holder.bind(productItem.chkItem, clickListener)
            }
            is TextViewHolder -> {
                val headerItem = getItem(position) as DataItem.Header
                holder.bind(headerItem.name)
            }
        }
    }

    class TextViewHolder(val binding: HeaderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(name: String) {
            binding.name = name
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): TextViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = HeaderBinding.inflate(layoutInflater, parent, false)
                return TextViewHolder(binding)
            }
        }
    }


    class ViewHolder private constructor(val binding: ListItemQuantityBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(chk_item: CheckoutItem, clickListener: ItemListener) {
            binding.chkItem = chk_item
            if (chk_item.quantity == 0){
                binding.buttonAddItem.visibility = View.VISIBLE
                binding.quantityButtons.visibility = View.INVISIBLE
            }else{
                binding.buttonAddItem.visibility = View.INVISIBLE
                binding.quantityButtons.visibility = View.VISIBLE
            }
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemQuantityBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class HomeItemDiffCallback : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
//        return newItem.quantity == newItem.quantity && oldItem.item == newItem.item
        return newItem == oldItem
    }
}

interface ItemListener {
    fun onPlus(itemId: Int)
    fun onMinus(itemId: Int)
}

sealed class DataItem {
    abstract val id: Int

    data class ProductItem(val chkItem: CheckoutItem) : DataItem() {
        override val id = chkItem.item.id
    }

    data class Header(val name: String, val number : Int) : DataItem() {
        override val id = number
    }

}