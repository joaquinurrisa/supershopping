package com.example.supershopping.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.supershopping.databinding.GridCartItemBinding
import com.example.supershopping.model.CheckoutItem


class CartItemAdapter(val clickListener: CartItemListener) : ListAdapter<CheckoutItem, CartItemAdapter.ViewHolder>(CartItemDiffCallback()) {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chkItem = getItem(position)
        holder.bind(chkItem,clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }


    class ViewHolder private constructor(val binding: GridCartItemBinding ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(chk_item: CheckoutItem, clickListener: CartItemListener) {
            binding.chkItem = chk_item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GridCartItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class CartItemDiffCallback : DiffUtil.ItemCallback<CheckoutItem>() {
    override fun areItemsTheSame(oldItem: CheckoutItem, newItem: CheckoutItem): Boolean {
        return oldItem.item.id == newItem.item.id
    }

    override fun areContentsTheSame(oldItem: CheckoutItem, newItem: CheckoutItem): Boolean {
        return newItem == oldItem
    }
}

interface CartItemListener {
    fun onItemClicked(chk_item: CheckoutItem)
}