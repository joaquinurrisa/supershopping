package com.example.supershopping.model

enum class ItemType(val id: Int) {
    FRUIT(-1),
    VEGGIE(-2),
    EXTRA(-3),
}