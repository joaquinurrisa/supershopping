package com.example.supershopping.model

class Banner (
    val title: String,
    val subtitle: String,
    val imgSrc: String
        )