package com.example.supershopping.model

data class CheckoutItem (
    val item: Item,
    var quantity: Int
)