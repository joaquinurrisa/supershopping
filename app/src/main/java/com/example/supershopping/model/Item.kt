package com.example.supershopping.model

data class Item(
    val id: Int,
    val name: String,
    val price: Float,
    val type: ItemType,
    val srcImgList: String,
    val srcImgCart: String,
)