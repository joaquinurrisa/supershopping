package com.example.supershopping.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.supershopping.fakedatabase.DataProvider

object Cart {

    private val chk_items : MutableList<CheckoutItem> = mutableListOf()

    private val _items = MutableLiveData<List<CheckoutItem>>()
    val items: LiveData<List<CheckoutItem>>
        get() = _items

    private val _total = MutableLiveData<Float>()
    val total : LiveData<Float>
        get() = _total


    init {
        loadItems(DataProvider.getItems())
        _items.value = chk_items.map { it.copy() }
        _items.value = chk_items
        _total.value = 0f
    }

    // Clears the current lists and adds the items as the new list
    private fun loadItems(items: List<Item>){
        clear()
        items.forEach {
            chk_items.add(CheckoutItem(it,0))
        }
    }

    // Adds one to the quantity of said item.
    // Updates total price according to item.price
    fun addOneToItemById(itemId:Int){
        val existentItem = chk_items.find { it.item.id == itemId }
        if(existentItem != null){
            existentItem.quantity++
            _items.value = chk_items.map { it.copy() }
            _total.value = _total.value?.plus(existentItem.item.price)
        }
    }
    fun modifyQuantityOfItemById(itemId:Int,newQuantity:Int){
        val existentItem = chk_items.find { it.item.id == itemId }
        if(existentItem != null && newQuantity >= 0){
            val valueDiference = (newQuantity - existentItem.quantity) * existentItem.item.price
            existentItem.quantity = newQuantity
            _items.value = chk_items.map { it.copy() }
            _total.value = _total.value?.plus(valueDiference)
        }
    }

    // Subtracts one to the quantity of item if quantity > 0
    // Updates total price according to item.price
    fun subtractOneToItemById(itemId:Int){
        val existentItem = chk_items.find { it.item.id == itemId }
        if(existentItem != null && existentItem.quantity > 0){
            existentItem.quantity--
            _items.value = chk_items.map { it.copy() }
            _total.value = _total.value?.minus(existentItem.item.price)
        }
    }

    // Removes all items and updates total price
    fun clear(){
        chk_items.clear()
        _items.value = listOf()
        _total.value = 0f
    }

    // Sets all quantities to 0 and updates total
    fun clearQuantities(){
        chk_items.forEach {
            it.quantity = 0
        }
        _items.value = chk_items.map { it.copy() }
        _total.value = 0f
    }

    fun refreshItems(){
        loadItems(DataProvider.getItems())
        _items.value = chk_items.map { it.copy() }
        _items.value = chk_items
        _total.value = 0f
    }

}