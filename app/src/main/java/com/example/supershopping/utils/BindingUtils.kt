package com.example.supershopping.utils

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.supershopping.R
import com.example.supershopping.model.CheckoutItem

@BindingAdapter("itemImage")
fun ImageView.SetItemImage(chk_item: CheckoutItem?){
    chk_item?.let{
        val imgUri = chk_item.item.srcImgList.toUri().buildUpon().scheme("https").build()
        Glide.with(this.context)
            .load(imgUri)
            .error(R.drawable.ic_broken_image)
            .into(this)
    }
}

@BindingAdapter("cartItemImage")
fun ImageView.SetCartItemImage(chk_item: CheckoutItem?){
    chk_item?.let{
        val imgUri = chk_item.item.srcImgCart.toUri().buildUpon().scheme("https").build()
        Glide.with(this.context)
            .load(imgUri)
            .error(R.drawable.ic_broken_image)
            .into(this)
    }
}


@BindingAdapter("itemName")
fun TextView.setItemName(chk_item: CheckoutItem?) {
    chk_item?.let {
        text = chk_item.item.name
    }
}

@BindingAdapter("itemPriceFormatted")
fun TextView.setItemPrice(chk_item: CheckoutItem?) {
    chk_item?.let {
        text = "$${chk_item.item.price}"
    }
}

@BindingAdapter("itemQuantity")
fun TextView.setItemQuantity(chk_item: CheckoutItem?) {
    chk_item?.let {
        text = chk_item.quantity.toString()
    }
}

@BindingAdapter("cartItemQuantity")
fun TextView.setCartItemQuantity(chk_item: CheckoutItem?) {
    chk_item?.let {
        text = "${chk_item.quantity.toString()} units"
    }
}