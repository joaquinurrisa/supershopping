package com.example.supershopping.viewmodel

import androidx.lifecycle.ViewModel
import com.example.supershopping.model.Cart
import com.example.supershopping.model.CheckoutItem

class CartViewModel :ViewModel() {

    val items = Cart.items
    val total = Cart.total


    fun getCartList(list: List<CheckoutItem>): List<CheckoutItem>{
        return list.filter { it.quantity>0 }
    }


    fun onClickCheckout() {
        Cart.clearQuantities()
    }
}