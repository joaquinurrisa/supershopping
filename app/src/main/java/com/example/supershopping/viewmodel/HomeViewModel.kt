package com.example.supershopping.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.supershopping.adapter.DataItem
import com.example.supershopping.fakedatabase.DataProvider
import com.example.supershopping.model.Cart
import com.example.supershopping.model.CheckoutItem
import com.example.supershopping.model.ItemType
import com.example.supershopping.network.SuperApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class HomeViewModel : ViewModel() {

    val items = Cart.items

    private val _filter = MutableLiveData<String>()
    val filter: LiveData<String>
        get() = _filter

    var filteredItems = items.value?.map { it.copy() }

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        _filter.value = ""
        getSuperProducts()
    }

    fun onClickPlus(itemId: Int) {
        Cart.addOneToItemById(itemId)
    }

    fun onClickMinus(itemId: Int) {
        Cart.subtractOneToItemById(itemId)
    }

    fun onClickModifyQuantity(itemId: Int, newQuantity: Int) {
        Cart.modifyQuantityOfItemById(itemId, newQuantity)
    }


    fun addHeaders(list: List<CheckoutItem>): List<DataItem> {
        var items = mutableListOf<DataItem>()
        ItemType.values().forEach { type ->
            val newList = list?.filter { it.item.type == type }
            if (!newList.isNullOrEmpty())
                items += listOf(
                    DataItem.Header(
                        type.toString().lowercase().replaceFirstChar { it.uppercase() },
                        type.id
                    )
                ) + newList.map { DataItem.ProductItem(it) }
        }
        return items.toList()
    }

    fun filterList() {
        filteredItems = items.value?.map { it.copy() }
        if (!filter.value.isNullOrEmpty()) {
            filteredItems = filteredItems?.filter {
                it.item.name.contains(Regex(filter.value!!, RegexOption.IGNORE_CASE)) ||
                        it.item.type.toString().contains(Regex(filter.value!!, RegexOption.IGNORE_CASE))
            }
        }

    }

    fun setFilter(filter: String) {
        _filter.value = filter
    }


    private fun getSuperProducts() {
        coroutineScope.launch {
            val getProductsDeferred = SuperApi.retrofitService.getProductsAsync()
            try {
                val listResult = getProductsDeferred.await()
                Log.i("DataProvider", "Response: $listResult")
                DataProvider.updateList(listResult)
                Cart.refreshItems()
            } catch (e: Exception) {
                Log.i("DataProvider", "Failure: ${e.message}")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}